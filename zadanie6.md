# Zadanie 6
## Zadanie 6a
```
A & B -> 3
A <- 1
E & F -> 9
B <- 3
A & D -> 6
A <- 1
A & B -> 3
A <- 1
A & C -> 4
```

W ogólnym rozwiązaniu należy rozpatrywać, czy koszt przejścia dwóch najszybszych osób + powrót najszybszej + przejście dwóch wolniejszych + powrót drugiej z kolei najszybciej jest mniejszy niż przechodzenie wolniejszej z najszybszą + powrót najszybszej.

W tym wypadku opłacało się to tylko raz z osobami E i F + A i B.

Odp: Najtańszy koszt pokonania mostu to 31.
## Zadanie 6b
W tym zdania zostało założone, że `a < b < c`, ponieważ rysunek na to wskazuje.
Oczywiście da radę rozpatrzeć inne konfiguracje. Jedyną różnicą w innych konfiguracjach jest człon `2[a-c][a-c]` 
powstały w wyniku rozwinięcia ze wzoru skróconego mnożenia.
![](zad6b.png)

## Zadanie 6c
Zadanie sprowadza się do policzenia średniej odległości wszystkich domów od początku drogi.
```
x = (1.1 + 2.3 + 3.0 + 4.2 + 5.1 + 6.3 + 13.4 + 19.1 + 27.5)/9 = 9.1[km]
```
Odp: Mieszkanie wynająłbym na 9.1 kilometrze. 
