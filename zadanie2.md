# Zadanie 2a

## Na stole leżą 4 karty. Każda z nich po jednej stronie ma literę a po drugiej liczbę.  Które karty musisz odwrócić aby przetestować regułę: „ Jeśli na jednej stronie jest wielka litera, na drugiej stronie znajduje się liczba parzysta?”
![](zad2-img.png)

Należy obrócić kartę `A` i `7`. Należy sprawdzić, czy za A znajduje się liczby parzysta oraz czy za liczba nieprzarzystą znajduje się mała litera.
1. Sprawdzamy, czy za kartą A jest liczba parzysta
2. Sprawdzamy, czy za kartą 7 nie ma wielkiej litery  


# Zadanie 2b

## Łódź motorowa pokonuje dystans z miasta A do miasta B w 2 godziny (z prądem) i z miasta B do miasta A w 6 godzin (pod prąd). Ile czasu zajęłoby dopłynięcie z miasta A do miasta B na tratwie?

```
Vm + Vp = s/2
Vm - Vp = s/6
```

Z tego wynika, że Vm = 2*Vp

```
Vm + Vp = s/2
Vm = 2*Vp
Vp = s/t
```

Z tego wynika, że `t = 6h`

Odp: Dopłynięcie na tratwie zajęłoby 6h.
