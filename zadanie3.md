# Zadanie 3a
## Cena biletu do parku rozrywki została obniżona; w rezultacie park odnotował 50-procentowy wzrost liczby odwiedzających. W tym samym czasie zyski ze sprzedaży biletów wzrosły o 20%. O jaki procent została zredukowana cena biletu? Zbuduj model tego problemu, wskaż istotne zmienne i podaj równania, które doprowadzą do rozwiązania.

```
x - cena biletu
y - liczba odwiedzających
z - zysk
```
Przed obniżką: `xy = z`

Po obniżce: `(1-d)x*1.5y = 1.2z`

Z tego wynika, że: `d = 0.2 = 20%`

Odp: Cena została zredukowana o 20%.

# Zadanie 3b
## Abacki, Babacki i Cabacki postanowili we własnym gronie rozegrać zawody lekkoatletyczne. Było kilka dyscyplin, w których startowali i w każdym z przypadków zwycięzca otrzymywał g punktów, drugi – s punktów, a ostatni, tj. trzeci – b punktów. Oczywiście g > s > b > 0. Ustalono też, że wszystkie trzy wartości: g, s i b są liczbami całkowitymi. Zawody zakończyły się i w żadnej z dyscyplin nie było remisów. Abacki zdobył 22 punkty, natomiast Babacki i Cabacki zebrali po 9 punktów każdy. Babacki wygrał skok w dal. Kto był drugi w wyścigu na 400 metrów? Zbuduj model tego problemu, wskaż istotne zmienne i podaj równania, które doprowadzą do rozwiązania.

```
g - punkty za pierwsze miejsce
y - punkty za drugie miejsce
z - punkty za trzecie miejsce
n - ilość konkurencji
```

`n(g+y+z) = 22+9+9 = 40`

Założenia wynikające z treści zadania:
- n, g, s, b są dodatnimi liczbami całkowitymi
- n >= 2
- g + s + z >= 6 

z założeń wynika, że istnieją następujące możliwe konifguacje:
```
n = 2 i g + s + b = 20
n = 4 i g + s + b = 10 
n = 5 i g + s + b = 8
```

### Rozważania dla `n = 2 i g + s + b = 20`:

W tej konfiguracji Babcki niemogłby zebrać 9pkt, wygrywając w jednej konkurencji

### Rozważania dla `n = 4 i g + s + b = 10`:

#### Możliwości
```
1) g = 7, s = 2, b = 1
2) g = 6, s = 3, b = 1
3) g = 5, s = 4, b = 1
4) g = 5, s = 3, b = 2
```
#### Weryfikacja
Jedyna konfiguracja, w której Babacki zdobył 9pkt, to `2)`
- Babacki `(g + 3b)`
- Abacki `(3g + s)`

Wynika z tego, że Cabacki musiał zebrać `3s + z`, a to daje w sumie 10pkt, dlatego odrzucamy tą możliwość

### Rozważania dla `n = 5 i g + s + b = 8`:
#### Możliwości
```
1) g = 5, s = 2, b = 1
2) g = 4, s = 3, b = 1
```
#### Weryfikacja
Jedyna konfiguracja, w której Abacki zdobył 22pkt, to `1)`
- Abacki `(4g + s)`
- Babacki `(g + 4b)`
- Cabacki `(4s + b)`

Z racji tego, że Babacki raz wygrał, a Abacki tylko raz nie wygrał i nigdy nie był trzeci, to
musiał być drugi w skoku w dal. Więc `Cabacki był drugi w biegu na 400m.`

