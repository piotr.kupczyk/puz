# Zadanie 5
## Zadanie 5a
### Cztery małżeństwa, pan i pani Biały, pan i pani Zielony, pan i pani Brązowy, oraz pan i pani Czarny, zasiedli przy okrągłym stole do obiadu. Usiedli przy stole w taki sposób, że panowie siedzieli na przemian z paniami. Ponadto małżonkowie żadnej pary małżeńskiej nie siedzieli ramię w ramię. Pani Biała usiadła między panem Zielonym i panem Czarnym, przy czym ten drugi siedział na prawo od pani Białej. Pan Biały usiadł obok pani Czarnej. Kto siedział na prawo od pani Zielonej?

Kolejność osób przy stole zgodnie z ruchem wskazówek zegara to:
- Pani Biała
- Pan Zielony
- Pani Brązowa
- Pan Biały
- Pani Czarna
- Pan Brązowy
- Pani Zielona
- Pan Czarny

Odp: Na prawo od Pani zielonej siedzi Pan Brązowy.

## Zadanie 5b
### Na wyspie są trzy plemiona (plemiona A, B i C) mieszkające w osobnych osadach. Powszechnie znanym jest fakt, że plemię A składa się z osób prawdomównych, członkowie plemienia B zaczynają rozmowę od zdania prawdziwego a potem zawsze kłamią, a członkowie plemienia C zaczynają rozmowę od zdania prawdziwego, a następnie na przemian kłamią i mówią prawdę. Pewnego dnia straż pożarna (dogodnie usytuowana pośrodku wyspy, aby łatwo mogła dotrzeć do każdej z trzech osad) otrzymała nagłe wezwanie od jednego z mieszkańców wyspy.
```
Rozmowa przebiegła tak:

- Pali się w jednej z osad!

- W której?

- W naszej!

- Która to?

- Osada C…

W tym momencie połączenie zostało przerwane. Do której osady powinna pojechać straż?
```

Po straż zadzwonił mieszkaniec wyspy B.

Nie mógłby to być mieszkaniec wyspy A, ponieważ zaprzecza sobie w 2 i 3 zdaniu, a jest zobligowany do mówienia zawsze prawdy.

Również nie mógłby być to mieszkaniec wyspy C, ponieważ jeżeli w 3 zdaniu powiedział prawdę, to drugie zdanie nie pasuje.

Odp: Straż powinna pojechać na wyspę A.

## Zadanie 5c
### Na końcu procesu ekscentryczny sędzia skazał oskarżonego na pewną liczbę lat więzienia. Podkreślił jednak, że oskarżony musi sam sobie „wybrać” liczbę lat odsiadki! Sędzia umieścił na obwodzie koła 12 pudełek, z których każde zostało ponumerowane. Patrząc zgodnie z ruchem wskazówek zegara, liczby umieszczone na pudełkach to, kolejno, 7, 10, 1, 3, 6, 11, 8, 4, 5, 9, 0 i 2. Skazanemu mężczyźnie powiedziano, że w każdym pudełku jest pewna liczba monet. Ponownie, patrząc zgodnie z ruchem wskazówek zegara, liczba monet w kolejnych pudełkach wynosiła 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 i 11. Skazany mężczyzna musiał wybrać pudełko, a liczba monet w tym pudełku określała długość jego odsiadki. Zanim jednak mężczyzna wskazał jakieś pudełko, spytał sędziego, czy może uzyskać pewną dodatkową informację, a mianowicie, ile pudełek ma numer równy liczbie znajdujących się w nim monet? Sędzia odpowiedział, że takie informacje nie mogą być ujawniane, gdyż pozwoliłyby mu określić puste pudełko. Mężczyzna pomyślał nieco i wskazał puste pudełko! Skąd wiedział? Które pudełko wskazał?

Sędzia odpowiadając przyznał, że istnieją pudełka, których index jest równy ilość lat do odsiadki.
W takim wypadku wystarczy znaleźć dwa pudełka spełniające równanie:
```
i_1 - index pierwszego pudełka
i_2 - index drugiego pudełka
d - odległość pomiędzy pudełkami

gdzie i_1 < i_2


i_1 + d = i_2
```

Odp: Wybrał pudełko numer 7.




