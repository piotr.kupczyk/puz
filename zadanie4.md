# Zadanie 4
## Zadanie 4a
### Na stole stało 100 dużych pudełek. Wybrano niektóre z nich i umieszczono w nich po 8 mniejszych pudełek. Następnie wybrano niektóre z tych mniejszych pudełek i umieszczono w nich po 8 jeszcze mniejszych pudełek. Ten proces został powtórzony kilka razy. Na koniec całego procesu pakowania na stole było 233 puste pudełka. Ile pudełek w sumie jest na stole?

![](zad4a.png)

Odp: Na stole są 252 pudełka.

## Zadanie 4b
### Dwóch złodziei skradło wartościowy naszyjnik z czarnych i białych pereł. Białych pereł jest tyle samo co czarnych. Liczba zarówno białych, jak i czarnych pereł jest parzysta. Perły są w naszyjniku równomiernie rozmieszczone, jednakże nie są ułożone kolorami na przemian ani też nawet symetrycznie. Podaj przekonujący argument, że nie ma znaczenia jak wygląda naszyjnik, bo zawsze jest możliwe takie jego przecięcie na dwie połowy, aby każda z nich miała połowę białych i połowę czarnych pereł. Przykład naszyjnika i jego podziału:

Ta własność wynika z tego, że liczba białych i czarnych pereł jest równa oraz parzysta, a każde rozważane cięcie dzieli naszyjnik 
na dwie równe części.



