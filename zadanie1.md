# Zagadki do wykładu 1:

## Zadanie 1a

```
Trzech turystów gotowało na kolację ryż. Pierwszy wsypał do garnka 400 g ryżu, a drugi – 200 g. Trzeci z nich nie miał ze sobą nic do jedzenia, więc zaproponował, że zapłaci im za posiłek 6$. Zakładając, że po ugotowaniu ryżu wszyscy trzej podzielili się jedzeniem po równo, powiedz jak powinni się dwaj pierwsi turyści podzielić pieniędzmi tego trzeciego, żeby było sprawiedliwie.
Uzasadnij rozwiązanie. Przekonaj, że twoje rozwiązanie jest sprawiedliwe.
```

Odp. Pierwszy turysta powinien wziąć dla siebie 6$.

Każdy z turystów zjał po 200g ryżu, co oznacza, że pierwszy dał trzeciemu 200g, a drugi zjał swoją procję 
i z nikim się nie dzielił. 

---

## Zadanie 1b

```
Abacki, Babacki i Cabacki mają w sumie sześcioro dzieci: pięciu chłopców i jedną dziewczynkę. 
Jan ma o jedno dziecko więcej niż Babacki. Tomasz ma tyle dzieci co Piotr i Jan razem. Cabacki ma tylu chłopców co Babacki.

Jak ma na imię Abacki i ilu ma chłopców?

Odpowiedź uzasadnij.
```

Moim zdaniem na podstawie powyższych danych nie można jednozancznie określić rozwiązania. Nie jest powiedziane, że Abacki, Babacki, Cabacki
mają na imie Tomasz, Jan lub Piotr. Z racji, że Cabacki i Babacki mają taką samą liczbę synów, to Abacki może mieć 5, 3 lub 1 syna/synów.

Natomiast rozwiązując test z nie zauważyłem takiej odpowiedzi, więc uznaje, że dodatkowym założeniem w zadaniu jest to, 
że imiona opdpowiadają nazwiskom.

Odp. 
Imie - Tomasz, Ilość synów - 3

Z racji tego, że Tomasz ma tyle dzieci co Jan i Piotr razem, a wszyscy razem mają 6, to Tomasz ma 3 dzieci.
Następnie z Jan = Babacki + 1 i Tomasz = Piotr + Jan wynika, że Babacki musi być Piotrem.
Więc Piotr Babacki ma jedno dziecko. 
Z racji tego, że Babacki ma jedno dziecko, a Babacki ma tyle samo chłopców co Cabacki, to
Babacki musi mieć syna. Gdybym to była dziewczynka, to Cabacki też nie mialby synów, a Abacki musiłby mięć 5 synów, co jest niemożliwe, bo Tomasz ma najwięcej dzieci i jest to 3.
Następnie skoro Cabacki też ma jednego syna, to zaczy, że zostało 3 chłpców i jedna dziewczynka. Wynika z tego, że
dziewczyna jest Jana, a skoro Jan ma jedno dziecko więcej od Babackiego, to Jan musi mieć na nazwisko Cabacki.
Więc drogą eliminacji zostaje tylko Abacki, który ma 3 dzieci (chłopców) i ma na imie Tomasz.
  